////////////////////////////
// Required
////////////////////////////

var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	rigger = require('gulp-rigger');

////////////////////////////
// HTML Task
////////////////////////////
gulp.task('html', function() {
	gulp.src(['app/build/*.html'])
	.pipe(rigger())
	.pipe(gulp.dest('app/'));
});

////////////////////////////
// Scripts Task
////////////////////////////
gulp.task('scripts', function() {
	gulp.src(['app/js/**/*.js', '!app/js/**/*.min.js'])
	.pipe(rename({suffix: '.min'}))
	.pipe(uglify())
	.pipe(gulp.dest('app/js'));
});

////////////////////////////
// Sass Task
////////////////////////////
gulp.task('sass', function() {
	gulp.src('app/sass/style.sass')
	.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
	.pipe(gulp.dest('app/css/'));
});

////////////////////////////
// Watch Task
////////////////////////////
gulp.task('watch', function() {
	gulp.watch('app/js/**/*.js', ['scripts']);
	gulp.watch('app/sass/**/*.sass', ['sass']);
	gulp.watch('app/template/**/*.html', ['html']);
	gulp.watch('app/build/**/*.html', ['html']);

});


////////////////////////////
// Default Task
////////////////////////////
gulp.task('default', ['scripts', 'watch', 'sass', 'html']);