// ui slider sidebar
  $( function() {
    $( "#slider-range-min" ).slider({
      range: "min",
      value: 1000,
      min: 1,
      max: 6000,
      slide: function( event, ui ) {
        $( "#amount" ).val(  ui.value +  "  ₽" );
      }
    });
    $( "#amount" ).val($( "#slider-range-min" ).slider( "value" )  +  "  ₽"  );
  } );

  $( function() {
    $( "#slider-range-min2" ).slider({
      range: "min",
      value: 875000,
      min: 6000,
      max: 875000,
      slide: function( event, ui ) {
        $( "#amount2" ).val(  ui.value +  "  ₽" );
      }
    });
    $( "#amount2" ).val($( "#slider-range-min2" ).slider( "value" )  +  "  ₽"  );
  } );


// selects
// $( function() {
 
 
//     // $( "#number" )
//     //   .selectmenu()
//     //   .selectmenu( "menuWidget" )
//     //     .addClass( "overflow" );
 
//     // $( "#brands" ).selectmenu();
//   } );


// lk hidden input

$(document).ready(function() { 
  var touch  = $('#lk__email');
  var input  = $('#lk__emailinp');
  
  $(touch).on('click', function(e) {
   e.preventDefault();
   input.fadeToggle();
  });
 });

var inputemail = document.getElementById('email__input');

  inputemail.oninput = function() {
    document.getElementById('email__result').innerHTML = inputemail.value;
  };


$(document).ready(function() { 
  var touch  = $('#lk__name');
  var input  = $('#lk__nameinp');
  
  $(touch).on('click', function(e) {
   e.preventDefault();
   input.fadeToggle();
  });
 });

var inputname = document.getElementById('name__input');

  inputname.oninput = function() {
    document.getElementById('name__result').innerHTML = inputname.value;
  };


$(document).ready(function() { 
  var touch  = $('#lk__tel');
  var input  = $('#lk__telinp');
  
  $(touch).on('click', function(e) {
   e.preventDefault();
   input.fadeToggle();
  });
 });

var inputtel = document.getElementById('tel__input');

  inputtel.oninput = function() {
    document.getElementById('tel__result').innerHTML = inputtel.value;
  };


$(document).ready(function() { 
  var touch  = $('#lk__cart');
  var input  = $('#lk__cartinp');
  
  $(touch).on('click', function(e) {
   e.preventDefault();
   input.fadeToggle();
  });
 });

var inpucart = document.getElementById('cart__input');

  inpucart.oninput = function() {
    document.getElementById('cart__result').innerHTML = inpucart.value;
  };

$(document).ready(function() { 
  var touch  = $('#lk__index');
  var input  = $('#lk__indexinp');
  
  $(touch).on('click', function(e) {
   e.preventDefault();
   input.fadeToggle();
  });
 });

var inputindex = document.getElementById('index__input');

  inputindex.oninput = function() {
    document.getElementById('index__result').innerHTML = inputindex.value;
  };

$(document).ready(function() { 
  var touch  = $('#lk__home');
  var input  = $('#lk__homeinp');
  
  $(touch).on('click', function(e) {
   e.preventDefault();
   input.fadeToggle();
  });
 });

var inputhome = document.getElementById('home__input');

  inputhome.oninput = function() {
    document.getElementById('home__result').innerHTML = inputhome.value;
  };

$(document).ready(function() { 
  var touch  = $('#lk__street');
  var input  = $('#lk__streetinp');
  
  $(touch).on('click', function(e) {
   e.preventDefault();
   input.fadeToggle();
  });
 });

var inputstreet = document.getElementById('street__input');

  inputstreet.oninput = function() {
    document.getElementById('street__result').innerHTML = inputstreet.value;
  };

$(document).ready(function() { 
  var touch  = $('#lk__aprt');
  var input  = $('#lk__aprtinp');
  
  $(touch).on('click', function(e) {
   e.preventDefault();
   input.fadeToggle();
  });
 });

var inputaprt = document.getElementById('aprt__input');

  inputaprt.oninput = function() {
    document.getElementById('aprt__result').innerHTML = inputaprt.value;
  };