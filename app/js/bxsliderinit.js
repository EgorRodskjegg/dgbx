$('#tabsmain a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
});

$('.bxslider').bxSlider({
  pagerCustom: '.bx-pager',
  controls: false
});

$('.item__bxslider').bxSlider({
  pagerCustom: '#bx-pager'
});

$(document).ready(function(){
  $('.logoslider__slider').bxSlider({
    slideWidth: 190,
    minSlides: 6,
    maxSlides: 6,
    moveSlides: 1,
    slideMargin: 0,
    pager: false
  });
});

