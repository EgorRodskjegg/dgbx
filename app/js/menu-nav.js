
 $(document).ready(function() { 
  var touch  = $('.header__menutoggle');
  var menu  = $('.navmenu');
  var span =$()
  
  $(touch).on('click', function(e) {
   e.preventDefault();
   menu.fadeToggle();
   menu.toggleClass('transition');
   touch.toggleClass('on')
  });


  $('.navmenu a').on('click', function() {
   $('.navmenu').removeClass('transition');
   var w = $(window).width();
   if(w < 574){
    $('.navmenu').fadeToggle('transition');
    $(touch).removeClass('on')
   } 
  });

  
 $(window).resize(function(){
   var w = $(window).width();
   if(w < 640 && menu.is(':hidden')) {
    menu.removeAttr('style');
   }
  });
 });
 